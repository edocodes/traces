
// If the collet is extended then the wafer table is never moving
check die_scratch :
	globally
		if the_collet_is_extended then the_wafer_table_is_not_moving

// In the trace it happens at least once that the collet is extended
check precondition_satisfied:
	finally the_collet_is_extended


// Define the signals that we need from the trace:
signal Collet_Pos : {'signal'='Collet_Pos'}

signal AM_Z_LEFT_P : {'signal'='AM_Z_LEFT_P'} 

signal AM_Z_RIGHT_P : {'signal'='AM_Z_RIGHT_P'} 

signal SS_X1_MOTOR_P : { 'signal'='SS_X1_MOTOR_P' }

signal SS_Y1_MOTOR_P : { 'signal'='SS_Y1_MOTOR_P' }


// Based on the speeds of the SS_X1 and SS_Y1 motors we can say whether the table is moving:
def the_wafer_table_is_not_moving :	( d SS_X1_MOTOR_P / dt == 0.0 and d SS_Y1_MOTOR_P / dt == 0.0 )


// Based on the collet position we can say which Z motor is relevant:
// look at left Z motor if collet position equals 1 or 5
def left_Z :
	( ( Collet_Pos >= 0.5 and Collet_Pos <= 1.5 )
	or
	( Collet_Pos >= 4.5 and Collet_Pos <= 5.5 ) )

// look at right Z motor if collet position equals 3 or 7
def right_Z :
	( ( Collet_Pos >= 2.5 and Collet_Pos <= 3.5 )
	or
	( Collet_Pos >= 6.5 and Collet_Pos <= 7.5 ) )

// Based on the proper Z motor and its position we define whether the collet is extended:
def the_collet_is_extended :
	(
		( left_Z and ( AM_Z_LEFT_P >= 0.00135 or AM_Z_LEFT_P <= -0.00135) )
	or
		( right_Z and ( AM_Z_RIGHT_P >= 0.00135 or AM_Z_RIGHT_P <= -0.00135) )
	)


